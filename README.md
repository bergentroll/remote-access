# remote-access

Скрипт, позволяющий пользователю контролировать удалённый доступ к его рабочему
столу. Когда утилита запущена, отображается окно предупреждения. VNC-демон
будет убит при закрытии окна. Значок для запуска следует искать в `Главное меню
-> Системные`.

## Сборка пакета

Войти в поддиректорию [greenatom-remote-access/](greenatom-remote-access/).
Выполнить `debuild -us -uc`. Будет собран неподписанный пакет и выполнена
проверка корректности при помощи `lintian`.
